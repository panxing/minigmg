#!/bin/bash

# for Edison ===================================================================
cc -O3 -fno-alias -fno-fnalias -xAVX   -openmp bench.c mg.c box.c operators.naive.c     timer.x86.c -D_MPI -D__PRINT_COMMUNICATION_BREAKDOWN -o run.cori.naive
cc -O3 -fno-alias -fno-fnalias -xAVX   -openmp bench.c mg.c box.c operators.reference.c timer.x86.c -D_MPI -D__PRINT_COMMUNICATION_BREAKDOWN -o run.cori.reference
cc -O3 -fno-alias -fno-fnalias -xAVX   -openmp bench.c mg.c box.c operators.reference.c timer.x86.c -D_MPI -D__PRINT_COMMUNICATION_BREAKDOWN -D__COLLABORATIVE_THREADING=6 -o run.cori.reference
cc -O3 -fno-alias -fno-fnalias -xAVX -dynamic -L. -lmpithor -openmp bench.c mg.c box.c operators.ompif.c   timer.x86.c -D_MPI -D_MPIThor -D__PRINT_COMMUNICATION_BREAKDOWN -D__COLLABORATIVE_THREADING=6 -o run.cori.ompif
cc -O3 -fno-alias -fno-fnalias -xAVX   -openmp bench.c mg.c box.c operators.ompif.c     timer.x86.c -D_MPI -D__PRINT_COMMUNICATION_BREAKDOWN -D__COLLABORATIVE_THREADING=6 -D__FUSION_RESIDUAL_RESTRICTION -o run.cori.fusion
cc -O3 -fno-alias -fno-fnalias -xAVX   -openmp bench.c mg.c box.c operators.sse.c       timer.x86.c -D_MPI -D__PRINT_COMMUNICATION_BREAKDOWN -D__COLLABORATIVE_THREADING=6 -D__FUSION_RESIDUAL_RESTRICTION -D__PREFETCH_NEXT_PLANE_FROM_DRAM -o run.cori.sse
cc -O3 -fno-alias -fno-fnalias -xAVX   -openmp bench.c mg.c box.c operators.avx.c       timer.x86.c -D_MPI -D__PRINT_COMMUNICATION_BREAKDOWN -D__COLLABORATIVE_THREADING=6 -D__FUSION_RESIDUAL_RESTRICTION -D__PREFETCH_NEXT_PLANE_FROM_DRAM -o run.cori.avx
