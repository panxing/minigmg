void buffer_copy(double * __restrict__ destination, double * __restrict__ source, int elements, int useCacheBypass){
  //__bcopy((void*)source,(void*)(destination),elements*sizeof(double));
  //if( (((uint64_t)source)&0x1F) || (((uint64_t)destination)&0x1F) || (elements&0x3) ){memcpy(destination,source,elements*sizeof(double));}
  if( (((uint64_t)source)&0x1F) || (((uint64_t)destination)&0x1F) || (elements&0x3) ){__bcopy((void*)source,(void*)(destination),elements*sizeof(double));}
//else{int c;for(c=0;c<elements;c+=4){vec_sta(vec_lda(0,(double*)(source+c)),0,(double*)(destination+c));}} 
  else{int c,elements8=elements<<3;for(c=0;c<elements8;c+=32){vec_sta(vec_lda(c,(double*)(source)),c,(double*)(destination));}} 
}
