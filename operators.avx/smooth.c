//==================================================================================================
void __box_smooth_GSRB_multiple(box_type *box, int phi_id, int rhs_id, double a, double b, double h, int sweep){
    int pencil = box->pencil;
    int plane = box->plane;
    int plane8   = plane<<3;
    int pencil8  = pencil<<3;
    int ghosts = box->ghosts;
    int DimI = box->dim.i;
    int DimJ = box->dim.j;
    int DimK = box->dim.k;
    double h2inv = 1.0/(h*h);
    double  * __restrict__ phi          = box->grids[  phi_id] + ghosts*plane;
    double  * __restrict__ rhs          = box->grids[  rhs_id] + ghosts*plane;
    double  * __restrict__ alpha        = box->grids[__alpha ] + ghosts*plane;
    double  * __restrict__ beta_i       = box->grids[__beta_i] + ghosts*plane;
    double  * __restrict__ beta_j       = box->grids[__beta_j] + ghosts*plane;
    double  * __restrict__ beta_k       = box->grids[__beta_k] + ghosts*plane;
    double  * __restrict__ lambda       = box->grids[__lambda] + ghosts*plane;
    uint64_t* __restrict__ RedBlackMask = box->RedBlackMask;
    const __m256d       a_splat4 =               _mm256_broadcast_sd(&a);
    const __m256d b_h2inv_splat4 = _mm256_mul_pd(_mm256_broadcast_sd(&b),_mm256_broadcast_sd(&h2inv));
    int global_ij_start[8];
    int global_ij_end[8];
    int ij_start[8];
    int ij_end[8];
    int planeInWavefront;for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){
      global_ij_start[planeInWavefront] = (                   (1+planeInWavefront)*pencil)&~3;
      global_ij_end[planeInWavefront]   = ((ghosts+DimJ+ghosts-1-planeInWavefront)*pencil);
      ij_start[planeInWavefront] = global_ij_start[planeInWavefront];
      ij_end[planeInWavefront]   = global_ij_end[planeInWavefront];
    }
   #if defined(__PREFETCH_NEXT_PLANE_FROM_DRAM)
   double * __restrict__ Prefetch_Pointers[20];
   Prefetch_Pointers[0] =    phi+plane-pencil;
   Prefetch_Pointers[1] = beta_k+plane       ;
   Prefetch_Pointers[2] = beta_j             ;
   Prefetch_Pointers[3] = beta_i             ;
   Prefetch_Pointers[4] =  alpha             ;
   Prefetch_Pointers[5] =    rhs             ;
   Prefetch_Pointers[6] = lambda             ;
   #endif
    int leadingK;
    int kLow  =     -(ghosts-1);
    int kHigh = DimK+(ghosts-1);
    for(leadingK=kLow-1;leadingK<kHigh;leadingK++){
      {
        #if defined(__PREFETCH_NEXT_PLANE_FROM_DRAM)
        int prefetch_stream=0;
        int prefetch_ijk_start = ij_start[0] + (leadingK+1)*plane;
        int prefetch_ijk_end   = ij_end[0]   + (leadingK+1)*plane;
        int prefetch_ijk       = prefetch_ijk_start;
        #endif
        int j,k;
        for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){
          k=(leadingK-planeInWavefront);if((k>=kLow)&&(k<kHigh)){
        uint64_t invertMask = 0-((k^planeInWavefront^sweep^1)&0x1);
        const __m256d    invertMask4 =               _mm256_broadcast_sd((double*)&invertMask);
        int ij,kplane=k*plane;
        int _ij_start    = ij_start[planeInWavefront];
        int _ij_end      = ij_end[planeInWavefront];
        for(ij=_ij_start;ij<_ij_end;ij+=16){ // smooth a vector...
          int ijk=ij+kplane;
          #if defined(__PREFETCH_NEXT_PLANE_FROM_DRAM)
          #warning will attempt to prefetch the next plane from DRAM one component at a time
            double * _base = Prefetch_Pointers[prefetch_stream] + prefetch_ijk;
            _mm_prefetch((const char*)(_base+ 0),_MM_HINT_T1);
            _mm_prefetch((const char*)(_base+ 8),_MM_HINT_T1);
            _mm_prefetch((const char*)(_base+16),_MM_HINT_T1);
            _mm_prefetch((const char*)(_base+24),_MM_HINT_T1);
            prefetch_ijk+=28;if(prefetch_ijk>prefetch_ijk_end){prefetch_stream++;prefetch_ijk=prefetch_ijk_start;}
          #endif
                __m256d helmholtz_00;
                __m256d helmholtz_04;
                __m256d helmholtz_08;
                __m256d helmholtz_12;
          #if 1 // this version performs alligned accesses for phi+/-1, but not betai+1 or phi+/-pencil
          const __m128d      temp_00 = _mm_load_pd(phi+ijk+ -2);
          const __m128d      temp_02 = _mm_load_pd(phi+ijk+  0);
          const __m128d      temp_01 = _mm_shuffle_pd(temp_00,temp_02,1);
          const __m128d      temp_04 = _mm_load_pd(phi+ijk+  2);
          const __m128d      temp_06 = _mm_load_pd(phi+ijk+  4);
          const __m128d      temp_03 = _mm_shuffle_pd(temp_02,temp_04,1);
          const __m128d      temp_05 = _mm_shuffle_pd(temp_04,temp_06,1);
          const __m256d       phi_00 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_02),temp_04,1);
          const __m256d       phi_m1 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_01),temp_03,1);
          const __m256d       phi_01 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_03),temp_05,1);
          const __m128d      temp_08 = _mm_load_pd(phi+ijk+  6);
          const __m128d      temp_10 = _mm_load_pd(phi+ijk+  8);
          const __m128d      temp_07 = _mm_shuffle_pd(temp_06,temp_08,1);
          const __m128d      temp_09 = _mm_shuffle_pd(temp_08,temp_10,1);
          const __m256d       phi_04 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_06),temp_08,1);
          const __m256d       phi_03 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_05),temp_07,1);
          const __m256d       phi_05 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_07),temp_09,1);
          const __m128d      temp_12 = _mm_load_pd(phi+ijk+ 10);
          const __m128d      temp_14 = _mm_load_pd(phi+ijk+ 12);
          const __m128d      temp_11 = _mm_shuffle_pd(temp_10,temp_12,1);
          const __m128d      temp_13 = _mm_shuffle_pd(temp_12,temp_14,1);
          const __m256d       phi_08 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_10),temp_12,1);
          const __m256d       phi_07 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_09),temp_11,1);
          const __m256d       phi_09 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_11),temp_13,1);
          const __m128d      temp_16 = _mm_load_pd(phi+ijk+ 14);
          const __m128d      temp_18 = _mm_load_pd(phi+ijk+ 16);
          const __m128d      temp_15 = _mm_shuffle_pd(temp_14,temp_16,1);
          const __m128d      temp_17 = _mm_shuffle_pd(temp_16,temp_18,1);
          const __m256d       phi_12 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_14),temp_16,1);
          const __m256d       phi_11 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_13),temp_15,1);
          const __m256d       phi_13 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_15),temp_17,1);
                        helmholtz_00 =                              _mm256_mul_pd(_mm256_sub_pd(phi_01,phi_00),_mm256_loadu_pd(beta_i+ijk+        1)); 
                        helmholtz_04 =                              _mm256_mul_pd(_mm256_sub_pd(phi_05,phi_04),_mm256_loadu_pd(beta_i+ijk+        5)); 
                        helmholtz_08 =                              _mm256_mul_pd(_mm256_sub_pd(phi_09,phi_08),_mm256_loadu_pd(beta_i+ijk+        9)); 
                        helmholtz_12 =                              _mm256_mul_pd(_mm256_sub_pd(phi_13,phi_12),_mm256_loadu_pd(beta_i+ijk+       13)); 
                        helmholtz_00 = _mm256_sub_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(phi_00,phi_m1),_mm256_load_pd( beta_i+ijk+        0)));
                        helmholtz_04 = _mm256_sub_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(phi_04,phi_03),_mm256_load_pd( beta_i+ijk+        4)));
                        helmholtz_08 = _mm256_sub_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(phi_08,phi_07),_mm256_load_pd( beta_i+ijk+        8)));
                        helmholtz_12 = _mm256_sub_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(phi_12,phi_11),_mm256_load_pd( beta_i+ijk+       12)));
                        //careful... assumes the compiler maps _mm256_load_pd to unaligned vmovupd and not the aligned version (should be faster when pencil is a multiple of 4 doubles (32 bytes)
                        helmholtz_00 = _mm256_add_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+pencil+ 0),                phi_00           ),_mm256_load_pd( beta_j+ijk+pencil+ 0)));
                        helmholtz_04 = _mm256_add_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+pencil+ 4),                phi_04           ),_mm256_load_pd( beta_j+ijk+pencil+ 4)));
                        helmholtz_08 = _mm256_add_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+pencil+ 8),                phi_08           ),_mm256_load_pd( beta_j+ijk+pencil+ 8)));
                        helmholtz_12 = _mm256_add_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+pencil+12),                phi_12           ),_mm256_load_pd( beta_j+ijk+pencil+12)));
                        helmholtz_00 = _mm256_sub_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(                phi_00           ,_mm256_load_pd( phi+ijk-pencil+ 0)),_mm256_load_pd( beta_j+ijk+        0)));
                        helmholtz_04 = _mm256_sub_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(                phi_04           ,_mm256_load_pd( phi+ijk-pencil+ 4)),_mm256_load_pd( beta_j+ijk+        4)));
                        helmholtz_08 = _mm256_sub_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(                phi_08           ,_mm256_load_pd( phi+ijk-pencil+ 8)),_mm256_load_pd( beta_j+ijk+        8)));
                        helmholtz_12 = _mm256_sub_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(                phi_12           ,_mm256_load_pd( phi+ijk-pencil+12)),_mm256_load_pd( beta_j+ijk+       12)));
                        helmholtz_00 = _mm256_add_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+ 0),                phi_00           ),_mm256_load_pd( beta_k+ijk+ plane+ 0)));
                        helmholtz_04 = _mm256_add_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+ 4),                phi_04           ),_mm256_load_pd( beta_k+ijk+ plane+ 4)));
                        helmholtz_08 = _mm256_add_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+ 8),                phi_08           ),_mm256_load_pd( beta_k+ijk+ plane+ 8)));
                        helmholtz_12 = _mm256_add_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+12),                phi_12           ),_mm256_load_pd( beta_k+ijk+ plane+12)));
                        helmholtz_00 = _mm256_sub_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(                phi_00           ,_mm256_load_pd( phi+ijk- plane+ 0)),_mm256_load_pd( beta_k+ijk       + 0)));
                        helmholtz_04 = _mm256_sub_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(                phi_04           ,_mm256_load_pd( phi+ijk- plane+ 4)),_mm256_load_pd( beta_k+ijk       + 4)));
                        helmholtz_08 = _mm256_sub_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(                phi_08           ,_mm256_load_pd( phi+ijk- plane+ 8)),_mm256_load_pd( beta_k+ijk       + 8)));
                        helmholtz_12 = _mm256_sub_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(                phi_12           ,_mm256_load_pd( phi+ijk- plane+12)),_mm256_load_pd( beta_k+ijk       +12)));
          #else // this version performs unalligned accesses for phi+/-1, betai+1 and phi+/-pencil
          const __m256d       phi_00 = _mm256_load_pd(phi+ijk+  0);
          const __m256d       phi_04 = _mm256_load_pd(phi+ijk+  4);
          const __m256d       phi_08 = _mm256_load_pd(phi+ijk+  8);
          const __m256d       phi_12 = _mm256_load_pd(phi+ijk+ 12);
                        //careful... assumes the compiler maps _mm256_load_pd to unaligned vmovupd and not the aligned version (should be faster when pencil is a multiple of 4 doubles (32 bytes)
                        helmholtz_00 =                              _mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd(phi+ijk+        1),                phi_00           ),_mm256_load_pd(beta_i+ijk+        1)); 
                        helmholtz_04 =                              _mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd(phi+ijk+        5),                phi_04           ),_mm256_load_pd(beta_i+ijk+        5)); 
                        helmholtz_08 =                              _mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd(phi+ijk+        9),                phi_08           ),_mm256_load_pd(beta_i+ijk+        9)); 
                        helmholtz_12 =                              _mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd(phi+ijk+       13),                phi_12           ),_mm256_load_pd(beta_i+ijk+       13)); 
                        helmholtz_00 = _mm256_sub_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(                phi_00           ,_mm256_load_pd(phi+ijk+       -1)),_mm256_load_pd( beta_i+ijk+        0)));
                        helmholtz_04 = _mm256_sub_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(                phi_04           ,_mm256_load_pd(phi+ijk+        3)),_mm256_load_pd( beta_i+ijk+        4)));
                        helmholtz_08 = _mm256_sub_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(                phi_08           ,_mm256_load_pd(phi+ijk+        7)),_mm256_load_pd( beta_i+ijk+        8)));
                        helmholtz_12 = _mm256_sub_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(                phi_12           ,_mm256_load_pd(phi+ijk+       11)),_mm256_load_pd( beta_i+ijk+       12)));
                        helmholtz_00 = _mm256_add_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(_mm256_loadu_pd( phi+ijk+pencil+ 0),                phi_00           ),_mm256_loadu_pd( beta_j+ijk+pencil+ 0)));
                        helmholtz_04 = _mm256_add_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(_mm256_loadu_pd( phi+ijk+pencil+ 4),                phi_04           ),_mm256_loadu_pd( beta_j+ijk+pencil+ 4)));
                        helmholtz_08 = _mm256_add_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(_mm256_loadu_pd( phi+ijk+pencil+ 8),                phi_08           ),_mm256_loadu_pd( beta_j+ijk+pencil+ 8)));
                        helmholtz_12 = _mm256_add_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(_mm256_loadu_pd( phi+ijk+pencil+12),                phi_12           ),_mm256_loadu_pd( beta_j+ijk+pencil+12)));
                        helmholtz_00 = _mm256_sub_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(                phi_00           ,_mm256_loadu_pd( phi+ijk-pencil+ 0)),_mm256_load_pd( beta_j+ijk+        0)));
                        helmholtz_04 = _mm256_sub_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(                phi_04           ,_mm256_loadu_pd( phi+ijk-pencil+ 4)),_mm256_load_pd( beta_j+ijk+        4)));
                        helmholtz_08 = _mm256_sub_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(                phi_08           ,_mm256_loadu_pd( phi+ijk-pencil+ 8)),_mm256_load_pd( beta_j+ijk+        8)));
                        helmholtz_12 = _mm256_sub_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(                phi_12           ,_mm256_loadu_pd( phi+ijk-pencil+12)),_mm256_load_pd( beta_j+ijk+       12)));
                        helmholtz_00 = _mm256_add_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+ 0),                phi_00           ),_mm256_load_pd( beta_k+ijk+ plane+ 0)));
                        helmholtz_04 = _mm256_add_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+ 4),                phi_04           ),_mm256_load_pd( beta_k+ijk+ plane+ 4)));
                        helmholtz_08 = _mm256_add_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+ 8),                phi_08           ),_mm256_load_pd( beta_k+ijk+ plane+ 8)));
                        helmholtz_12 = _mm256_add_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+12),                phi_12           ),_mm256_load_pd( beta_k+ijk+ plane+12)));
                        helmholtz_00 = _mm256_sub_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(                phi_00           ,_mm256_load_pd( phi+ijk- plane+ 0)),_mm256_load_pd( beta_k+ijk       + 0)));
                        helmholtz_04 = _mm256_sub_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(                phi_04           ,_mm256_load_pd( phi+ijk- plane+ 4)),_mm256_load_pd( beta_k+ijk       + 4)));
                        helmholtz_08 = _mm256_sub_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(                phi_08           ,_mm256_load_pd( phi+ijk- plane+ 8)),_mm256_load_pd( beta_k+ijk       + 8)));
                        helmholtz_12 = _mm256_sub_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(                phi_12           ,_mm256_load_pd( phi+ijk- plane+12)),_mm256_load_pd( beta_k+ijk       +12)));
          #endif
                        helmholtz_00 = _mm256_mul_pd(helmholtz_00,b_h2inv_splat4);
                        helmholtz_04 = _mm256_mul_pd(helmholtz_04,b_h2inv_splat4);
                        helmholtz_08 = _mm256_mul_pd(helmholtz_08,b_h2inv_splat4);
                        helmholtz_12 = _mm256_mul_pd(helmholtz_12,b_h2inv_splat4);
                        helmholtz_00 = _mm256_sub_pd(_mm256_mul_pd(_mm256_mul_pd(a_splat4,_mm256_load_pd(alpha+ijk+ 0)),phi_00),helmholtz_00);
                        helmholtz_04 = _mm256_sub_pd(_mm256_mul_pd(_mm256_mul_pd(a_splat4,_mm256_load_pd(alpha+ijk+ 4)),phi_04),helmholtz_04);
                        helmholtz_08 = _mm256_sub_pd(_mm256_mul_pd(_mm256_mul_pd(a_splat4,_mm256_load_pd(alpha+ijk+ 8)),phi_08),helmholtz_08);
                        helmholtz_12 = _mm256_sub_pd(_mm256_mul_pd(_mm256_mul_pd(a_splat4,_mm256_load_pd(alpha+ijk+12)),phi_12),helmholtz_12);
                __m256d       new_00 = _mm256_mul_pd(_mm256_load_pd(lambda+ijk+ 0),_mm256_sub_pd(helmholtz_00,_mm256_load_pd(rhs+ijk+ 0)));
                __m256d       new_04 = _mm256_mul_pd(_mm256_load_pd(lambda+ijk+ 4),_mm256_sub_pd(helmholtz_04,_mm256_load_pd(rhs+ijk+ 4)));
                __m256d       new_08 = _mm256_mul_pd(_mm256_load_pd(lambda+ijk+ 8),_mm256_sub_pd(helmholtz_08,_mm256_load_pd(rhs+ijk+ 8)));
                __m256d       new_12 = _mm256_mul_pd(_mm256_load_pd(lambda+ijk+12),_mm256_sub_pd(helmholtz_12,_mm256_load_pd(rhs+ijk+12)));
                              new_00 = _mm256_sub_pd(phi_00,new_00);
                              new_04 = _mm256_sub_pd(phi_04,new_04);
                              new_08 = _mm256_sub_pd(phi_08,new_08);
                              new_12 = _mm256_sub_pd(phi_12,new_12);
           const __m256d RedBlack_00 = _mm256_xor_pd(invertMask4,_mm256_castsi256_pd(_mm256_load_si256( (__m256i*)(RedBlackMask+ij+ 0) )));
           const __m256d RedBlack_04 = _mm256_xor_pd(invertMask4,_mm256_castsi256_pd(_mm256_load_si256( (__m256i*)(RedBlackMask+ij+ 4) )));
           const __m256d RedBlack_08 = _mm256_xor_pd(invertMask4,_mm256_castsi256_pd(_mm256_load_si256( (__m256i*)(RedBlackMask+ij+ 8) )));
           const __m256d RedBlack_12 = _mm256_xor_pd(invertMask4,_mm256_castsi256_pd(_mm256_load_si256( (__m256i*)(RedBlackMask+ij+12) )));
                             new_00  = _mm256_blendv_pd(phi_00,new_00,RedBlack_00);
                             new_04  = _mm256_blendv_pd(phi_04,new_04,RedBlack_04);
                             new_08  = _mm256_blendv_pd(phi_08,new_08,RedBlack_08);
                             new_12  = _mm256_blendv_pd(phi_12,new_12,RedBlack_12);
                                         _mm256_store_pd(phi+ijk+ 0,new_00);
                                         _mm256_store_pd(phi+ijk+ 4,new_04);
                                         _mm256_store_pd(phi+ijk+ 8,new_08);
                                         _mm256_store_pd(phi+ijk+12,new_12);
        }
        }}
      } // if stencil
    } // leadingK
}


void __box_smooth_GSRB_multiple_threaded(box_type *box, int phi_id, int rhs_id, double a, double b, double h, int sweep){
  #pragma omp parallel
  {
    int pencil = box->pencil;
    int plane = box->plane;
    int plane8   = plane<<3;
    int pencil8  = pencil<<3;
    int ghosts = box->ghosts;
    int DimI = box->dim.i;
    int DimJ = box->dim.j;
    int DimK = box->dim.k;
    double h2inv = 1.0/(h*h);
    double  * __restrict__ phi          = box->grids[  phi_id] + ghosts*plane;
    double  * __restrict__ rhs          = box->grids[  rhs_id] + ghosts*plane;
    double  * __restrict__ alpha        = box->grids[__alpha ] + ghosts*plane;
    double  * __restrict__ beta_i       = box->grids[__beta_i] + ghosts*plane;
    double  * __restrict__ beta_j       = box->grids[__beta_j] + ghosts*plane;
    double  * __restrict__ beta_k       = box->grids[__beta_k] + ghosts*plane;
    double  * __restrict__ lambda       = box->grids[__lambda] + ghosts*plane;
    uint64_t* __restrict__ RedBlackMask = box->RedBlackMask;
    const __m256d       a_splat4 =               _mm256_broadcast_sd(&a);
    const __m256d b_h2inv_splat4 = _mm256_mul_pd(_mm256_broadcast_sd(&b),_mm256_broadcast_sd(&h2inv));
    int id      = omp_get_thread_num();
    int threads = omp_get_num_threads();
    int DRAM_scout_threads = 0;
    int global_ij_start[8];
    int global_ij_end[8];
    int ij_start[8];
    int ij_end[8];
    int planeInWavefront;for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){
      global_ij_start[planeInWavefront] = (                   (1)*pencil)&~3;
      global_ij_end[planeInWavefront]   = ((ghosts+DimJ+ghosts-1)*pencil);
      int TotalUnrollings = ((global_ij_end[planeInWavefront]-global_ij_start[planeInWavefront]+16-1)/16);
      ij_start[planeInWavefront] = global_ij_start[planeInWavefront] + 16*( (id          )*(TotalUnrollings)/(threads-DRAM_scout_threads));
      ij_end[planeInWavefront]   = global_ij_start[planeInWavefront] + 16*( (id+1        )*(TotalUnrollings)/(threads-DRAM_scout_threads));
      if(ij_end[planeInWavefront]>global_ij_end[planeInWavefront])ij_end[planeInWavefront]=global_ij_end[planeInWavefront];
    }
   #if defined(__PREFETCH_NEXT_PLANE_FROM_DRAM)
   double * __restrict__ Prefetch_Pointers[20];
   Prefetch_Pointers[0] =    phi+plane-pencil;
   Prefetch_Pointers[1] = beta_k+plane       ;
   Prefetch_Pointers[2] = beta_j             ;
   Prefetch_Pointers[3] = beta_i             ;
   Prefetch_Pointers[4] =  alpha             ;
   Prefetch_Pointers[5] =    rhs             ;
   Prefetch_Pointers[6] = lambda             ;
   #endif
    int leadingK;
    int kLow  =     -(ghosts-1);
    int kHigh = DimK+(ghosts-1);
    for(leadingK=kLow-1;leadingK<kHigh;leadingK++){
      if(ghosts>1){
        #pragma omp barrier
      }
      {
        #if defined(__PREFETCH_NEXT_PLANE_FROM_DRAM)
        int prefetch_stream=0;
        int prefetch_ijk_start = ij_start[0] + (leadingK+1)*plane;
        int prefetch_ijk_end   = ij_end[0]   + (leadingK+1)*plane;
        int prefetch_ijk       = prefetch_ijk_start;
        #endif
        int j,k;
        for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){
          k=(leadingK-planeInWavefront);if((k>=kLow)&&(k<kHigh)){
        uint64_t invertMask = 0-((k^planeInWavefront^sweep^1)&0x1);
        const __m256d    invertMask4 =               _mm256_broadcast_sd((double*)&invertMask);
        int ij,kplane=k*plane;
        int _ij_start    = ij_start[planeInWavefront];
        int _ij_end      = ij_end[planeInWavefront];
        for(ij=_ij_start;ij<_ij_end;ij+=16){ // smooth a vector...
          int ijk=ij+kplane;
          #if defined(__PREFETCH_NEXT_PLANE_FROM_DRAM)
          #warning will attempt to prefetch the next plane from DRAM one component at a time
            double * _base = Prefetch_Pointers[prefetch_stream] + prefetch_ijk;
            _mm_prefetch((const char*)(_base+ 0),_MM_HINT_T1);
            _mm_prefetch((const char*)(_base+ 8),_MM_HINT_T1);
            _mm_prefetch((const char*)(_base+16),_MM_HINT_T1);
            _mm_prefetch((const char*)(_base+24),_MM_HINT_T1);
            prefetch_ijk+=28;if(prefetch_ijk>prefetch_ijk_end){prefetch_stream++;prefetch_ijk=prefetch_ijk_start;}
          #endif
                __m256d helmholtz_00;
                __m256d helmholtz_04;
                __m256d helmholtz_08;
                __m256d helmholtz_12;
          #if 1 // this version performs alligned accesses for phi+/-1, but not betai+1 or phi+/-pencil
          const __m128d      temp_00 = _mm_load_pd(phi+ijk+ -2);
          const __m128d      temp_02 = _mm_load_pd(phi+ijk+  0);
          const __m128d      temp_01 = _mm_shuffle_pd(temp_00,temp_02,1);
          const __m128d      temp_04 = _mm_load_pd(phi+ijk+  2);
          const __m128d      temp_06 = _mm_load_pd(phi+ijk+  4);
          const __m128d      temp_03 = _mm_shuffle_pd(temp_02,temp_04,1);
          const __m128d      temp_05 = _mm_shuffle_pd(temp_04,temp_06,1);
          const __m256d       phi_00 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_02),temp_04,1);
          const __m256d       phi_m1 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_01),temp_03,1);
          const __m256d       phi_01 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_03),temp_05,1);
          const __m128d      temp_08 = _mm_load_pd(phi+ijk+  6);
          const __m128d      temp_10 = _mm_load_pd(phi+ijk+  8);
          const __m128d      temp_07 = _mm_shuffle_pd(temp_06,temp_08,1);
          const __m128d      temp_09 = _mm_shuffle_pd(temp_08,temp_10,1);
          const __m256d       phi_04 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_06),temp_08,1);
          const __m256d       phi_03 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_05),temp_07,1);
          const __m256d       phi_05 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_07),temp_09,1);
          const __m128d      temp_12 = _mm_load_pd(phi+ijk+ 10);
          const __m128d      temp_14 = _mm_load_pd(phi+ijk+ 12);
          const __m128d      temp_11 = _mm_shuffle_pd(temp_10,temp_12,1);
          const __m128d      temp_13 = _mm_shuffle_pd(temp_12,temp_14,1);
          const __m256d       phi_08 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_10),temp_12,1);
          const __m256d       phi_07 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_09),temp_11,1);
          const __m256d       phi_09 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_11),temp_13,1);
          const __m128d      temp_16 = _mm_load_pd(phi+ijk+ 14);
          const __m128d      temp_18 = _mm_load_pd(phi+ijk+ 16);
          const __m128d      temp_15 = _mm_shuffle_pd(temp_14,temp_16,1);
          const __m128d      temp_17 = _mm_shuffle_pd(temp_16,temp_18,1);
          const __m256d       phi_12 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_14),temp_16,1);
          const __m256d       phi_11 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_13),temp_15,1);
          const __m256d       phi_13 = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_15),temp_17,1);
                        helmholtz_00 =                              _mm256_mul_pd(_mm256_sub_pd(phi_01,phi_00),_mm256_loadu_pd(beta_i+ijk+        1)); 
                        helmholtz_04 =                              _mm256_mul_pd(_mm256_sub_pd(phi_05,phi_04),_mm256_loadu_pd(beta_i+ijk+        5)); 
                        helmholtz_08 =                              _mm256_mul_pd(_mm256_sub_pd(phi_09,phi_08),_mm256_loadu_pd(beta_i+ijk+        9)); 
                        helmholtz_12 =                              _mm256_mul_pd(_mm256_sub_pd(phi_13,phi_12),_mm256_loadu_pd(beta_i+ijk+       13)); 
                        helmholtz_00 = _mm256_sub_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(phi_00,phi_m1),_mm256_load_pd( beta_i+ijk+        0)));
                        helmholtz_04 = _mm256_sub_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(phi_04,phi_03),_mm256_load_pd( beta_i+ijk+        4)));
                        helmholtz_08 = _mm256_sub_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(phi_08,phi_07),_mm256_load_pd( beta_i+ijk+        8)));
                        helmholtz_12 = _mm256_sub_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(phi_12,phi_11),_mm256_load_pd( beta_i+ijk+       12)));
                        //careful... assumes the compiler maps _mm256_load_pd to unaligned vmovupd and not the aligned version (should be faster when pencil is a multiple of 4 doubles (32 bytes)
                        helmholtz_00 = _mm256_add_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+pencil+ 0),                phi_00           ),_mm256_load_pd( beta_j+ijk+pencil+ 0)));
                        helmholtz_04 = _mm256_add_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+pencil+ 4),                phi_04           ),_mm256_load_pd( beta_j+ijk+pencil+ 4)));
                        helmholtz_08 = _mm256_add_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+pencil+ 8),                phi_08           ),_mm256_load_pd( beta_j+ijk+pencil+ 8)));
                        helmholtz_12 = _mm256_add_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+pencil+12),                phi_12           ),_mm256_load_pd( beta_j+ijk+pencil+12)));
                        helmholtz_00 = _mm256_sub_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(                phi_00           ,_mm256_load_pd( phi+ijk-pencil+ 0)),_mm256_load_pd( beta_j+ijk+        0)));
                        helmholtz_04 = _mm256_sub_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(                phi_04           ,_mm256_load_pd( phi+ijk-pencil+ 4)),_mm256_load_pd( beta_j+ijk+        4)));
                        helmholtz_08 = _mm256_sub_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(                phi_08           ,_mm256_load_pd( phi+ijk-pencil+ 8)),_mm256_load_pd( beta_j+ijk+        8)));
                        helmholtz_12 = _mm256_sub_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(                phi_12           ,_mm256_load_pd( phi+ijk-pencil+12)),_mm256_load_pd( beta_j+ijk+       12)));
                        helmholtz_00 = _mm256_add_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+ 0),                phi_00           ),_mm256_load_pd( beta_k+ijk+ plane+ 0)));
                        helmholtz_04 = _mm256_add_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+ 4),                phi_04           ),_mm256_load_pd( beta_k+ijk+ plane+ 4)));
                        helmholtz_08 = _mm256_add_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+ 8),                phi_08           ),_mm256_load_pd( beta_k+ijk+ plane+ 8)));
                        helmholtz_12 = _mm256_add_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+12),                phi_12           ),_mm256_load_pd( beta_k+ijk+ plane+12)));
                        helmholtz_00 = _mm256_sub_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(                phi_00           ,_mm256_load_pd( phi+ijk- plane+ 0)),_mm256_load_pd( beta_k+ijk       + 0)));
                        helmholtz_04 = _mm256_sub_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(                phi_04           ,_mm256_load_pd( phi+ijk- plane+ 4)),_mm256_load_pd( beta_k+ijk       + 4)));
                        helmholtz_08 = _mm256_sub_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(                phi_08           ,_mm256_load_pd( phi+ijk- plane+ 8)),_mm256_load_pd( beta_k+ijk       + 8)));
                        helmholtz_12 = _mm256_sub_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(                phi_12           ,_mm256_load_pd( phi+ijk- plane+12)),_mm256_load_pd( beta_k+ijk       +12)));
          #else // this version performs unalligned accesses for phi+/-1, betai+1 and phi+/-pencil
          const __m256d       phi_00 = _mm256_load_pd(phi+ijk+  0);
          const __m256d       phi_04 = _mm256_load_pd(phi+ijk+  4);
          const __m256d       phi_08 = _mm256_load_pd(phi+ijk+  8);
          const __m256d       phi_12 = _mm256_load_pd(phi+ijk+ 12);
                        //careful... assumes the compiler maps _mm256_load_pd to unaligned vmovupd and not the aligned version (should be faster when pencil is a multiple of 4 doubles (32 bytes)
                        helmholtz_00 =                              _mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd(phi+ijk+        1),                phi_00           ),_mm256_load_pd(beta_i+ijk+        1)); 
                        helmholtz_04 =                              _mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd(phi+ijk+        5),                phi_04           ),_mm256_load_pd(beta_i+ijk+        5)); 
                        helmholtz_08 =                              _mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd(phi+ijk+        9),                phi_08           ),_mm256_load_pd(beta_i+ijk+        9)); 
                        helmholtz_12 =                              _mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd(phi+ijk+       13),                phi_12           ),_mm256_load_pd(beta_i+ijk+       13)); 
                        helmholtz_00 = _mm256_sub_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(                phi_00           ,_mm256_load_pd(phi+ijk+       -1)),_mm256_load_pd( beta_i+ijk+        0)));
                        helmholtz_04 = _mm256_sub_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(                phi_04           ,_mm256_load_pd(phi+ijk+        3)),_mm256_load_pd( beta_i+ijk+        4)));
                        helmholtz_08 = _mm256_sub_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(                phi_08           ,_mm256_load_pd(phi+ijk+        7)),_mm256_load_pd( beta_i+ijk+        8)));
                        helmholtz_12 = _mm256_sub_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(                phi_12           ,_mm256_load_pd(phi+ijk+       11)),_mm256_load_pd( beta_i+ijk+       12)));
                        helmholtz_00 = _mm256_add_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(_mm256_loadu_pd( phi+ijk+pencil+ 0),                phi_00           ),_mm256_loadu_pd( beta_j+ijk+pencil+ 0)));
                        helmholtz_04 = _mm256_add_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(_mm256_loadu_pd( phi+ijk+pencil+ 4),                phi_04           ),_mm256_loadu_pd( beta_j+ijk+pencil+ 4)));
                        helmholtz_08 = _mm256_add_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(_mm256_loadu_pd( phi+ijk+pencil+ 8),                phi_08           ),_mm256_loadu_pd( beta_j+ijk+pencil+ 8)));
                        helmholtz_12 = _mm256_add_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(_mm256_loadu_pd( phi+ijk+pencil+12),                phi_12           ),_mm256_loadu_pd( beta_j+ijk+pencil+12)));
                        helmholtz_00 = _mm256_sub_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(                phi_00           ,_mm256_loadu_pd( phi+ijk-pencil+ 0)),_mm256_load_pd( beta_j+ijk+        0)));
                        helmholtz_04 = _mm256_sub_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(                phi_04           ,_mm256_loadu_pd( phi+ijk-pencil+ 4)),_mm256_load_pd( beta_j+ijk+        4)));
                        helmholtz_08 = _mm256_sub_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(                phi_08           ,_mm256_loadu_pd( phi+ijk-pencil+ 8)),_mm256_load_pd( beta_j+ijk+        8)));
                        helmholtz_12 = _mm256_sub_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(                phi_12           ,_mm256_loadu_pd( phi+ijk-pencil+12)),_mm256_load_pd( beta_j+ijk+       12)));
                        helmholtz_00 = _mm256_add_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+ 0),                phi_00           ),_mm256_load_pd( beta_k+ijk+ plane+ 0)));
                        helmholtz_04 = _mm256_add_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+ 4),                phi_04           ),_mm256_load_pd( beta_k+ijk+ plane+ 4)));
                        helmholtz_08 = _mm256_add_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+ 8),                phi_08           ),_mm256_load_pd( beta_k+ijk+ plane+ 8)));
                        helmholtz_12 = _mm256_add_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+12),                phi_12           ),_mm256_load_pd( beta_k+ijk+ plane+12)));
                        helmholtz_00 = _mm256_sub_pd(helmholtz_00,_mm256_mul_pd(_mm256_sub_pd(                phi_00           ,_mm256_load_pd( phi+ijk- plane+ 0)),_mm256_load_pd( beta_k+ijk       + 0)));
                        helmholtz_04 = _mm256_sub_pd(helmholtz_04,_mm256_mul_pd(_mm256_sub_pd(                phi_04           ,_mm256_load_pd( phi+ijk- plane+ 4)),_mm256_load_pd( beta_k+ijk       + 4)));
                        helmholtz_08 = _mm256_sub_pd(helmholtz_08,_mm256_mul_pd(_mm256_sub_pd(                phi_08           ,_mm256_load_pd( phi+ijk- plane+ 8)),_mm256_load_pd( beta_k+ijk       + 8)));
                        helmholtz_12 = _mm256_sub_pd(helmholtz_12,_mm256_mul_pd(_mm256_sub_pd(                phi_12           ,_mm256_load_pd( phi+ijk- plane+12)),_mm256_load_pd( beta_k+ijk       +12)));
          #endif
                        helmholtz_00 = _mm256_mul_pd(helmholtz_00,b_h2inv_splat4);
                        helmholtz_04 = _mm256_mul_pd(helmholtz_04,b_h2inv_splat4);
                        helmholtz_08 = _mm256_mul_pd(helmholtz_08,b_h2inv_splat4);
                        helmholtz_12 = _mm256_mul_pd(helmholtz_12,b_h2inv_splat4);
                        helmholtz_00 = _mm256_sub_pd(_mm256_mul_pd(_mm256_mul_pd(a_splat4,_mm256_load_pd(alpha+ijk+ 0)),phi_00),helmholtz_00);
                        helmholtz_04 = _mm256_sub_pd(_mm256_mul_pd(_mm256_mul_pd(a_splat4,_mm256_load_pd(alpha+ijk+ 4)),phi_04),helmholtz_04);
                        helmholtz_08 = _mm256_sub_pd(_mm256_mul_pd(_mm256_mul_pd(a_splat4,_mm256_load_pd(alpha+ijk+ 8)),phi_08),helmholtz_08);
                        helmholtz_12 = _mm256_sub_pd(_mm256_mul_pd(_mm256_mul_pd(a_splat4,_mm256_load_pd(alpha+ijk+12)),phi_12),helmholtz_12);
                __m256d       new_00 = _mm256_mul_pd(_mm256_load_pd(lambda+ijk+ 0),_mm256_sub_pd(helmholtz_00,_mm256_load_pd(rhs+ijk+ 0)));
                __m256d       new_04 = _mm256_mul_pd(_mm256_load_pd(lambda+ijk+ 4),_mm256_sub_pd(helmholtz_04,_mm256_load_pd(rhs+ijk+ 4)));
                __m256d       new_08 = _mm256_mul_pd(_mm256_load_pd(lambda+ijk+ 8),_mm256_sub_pd(helmholtz_08,_mm256_load_pd(rhs+ijk+ 8)));
                __m256d       new_12 = _mm256_mul_pd(_mm256_load_pd(lambda+ijk+12),_mm256_sub_pd(helmholtz_12,_mm256_load_pd(rhs+ijk+12)));
                              new_00 = _mm256_sub_pd(phi_00,new_00);
                              new_04 = _mm256_sub_pd(phi_04,new_04);
                              new_08 = _mm256_sub_pd(phi_08,new_08);
                              new_12 = _mm256_sub_pd(phi_12,new_12);
           const __m256d RedBlack_00 = _mm256_xor_pd(invertMask4,_mm256_castsi256_pd(_mm256_load_si256( (__m256i*)(RedBlackMask+ij+ 0) )));
           const __m256d RedBlack_04 = _mm256_xor_pd(invertMask4,_mm256_castsi256_pd(_mm256_load_si256( (__m256i*)(RedBlackMask+ij+ 4) )));
           const __m256d RedBlack_08 = _mm256_xor_pd(invertMask4,_mm256_castsi256_pd(_mm256_load_si256( (__m256i*)(RedBlackMask+ij+ 8) )));
           const __m256d RedBlack_12 = _mm256_xor_pd(invertMask4,_mm256_castsi256_pd(_mm256_load_si256( (__m256i*)(RedBlackMask+ij+12) )));
                             new_00  = _mm256_blendv_pd(phi_00,new_00,RedBlack_00);
                             new_04  = _mm256_blendv_pd(phi_04,new_04,RedBlack_04);
                             new_08  = _mm256_blendv_pd(phi_08,new_08,RedBlack_08);
                             new_12  = _mm256_blendv_pd(phi_12,new_12,RedBlack_12);
                                         _mm256_store_pd(phi+ijk+ 0,new_00);
                                         _mm256_store_pd(phi+ijk+ 4,new_04);
                                         _mm256_store_pd(phi+ijk+ 8,new_08);
                                         _mm256_store_pd(phi+ijk+12,new_12);
        }
        }}
      } // if stencil
    } // leadingK
  } // omp parallel region
}


//==================================================================================================
void smooth(domain_type * domain, int level, int phi_id, int rhs_id, double a, double b, double hLevel, int sweep){
  int CollaborativeThreadingBoxSize = 100000; // i.e. never
  #ifdef __COLLABORATIVE_THREADING
    #warning using Collaborative Threading for large boxes...
    CollaborativeThreadingBoxSize = 1 << __COLLABORATIVE_THREADING;
  #endif
  int box;
  if(domain->subdomains[0].levels[level].dim.i >= CollaborativeThreadingBoxSize){
    for(box=0;box<domain->numsubdomains;box++){__box_smooth_GSRB_multiple_threaded(&domain->subdomains[box].levels[level],phi_id,rhs_id,a,b,hLevel,sweep);}
  }else{
    #pragma omp parallel for private(box)
    for(box=0;box<domain->numsubdomains;box++){__box_smooth_GSRB_multiple(&domain->subdomains[box].levels[level],phi_id,rhs_id,a,b,hLevel,sweep);}
  }
}
//==================================================================================================
