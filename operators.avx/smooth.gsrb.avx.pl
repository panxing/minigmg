eval 'exec perl $0 $*'
        if 0;
$NumArgs = $#ARGV+1;

$ARGV0 = $ARGV[0];
$ARGV0 =~ tr/A-Z/a-z/;

#==========================================================================================================================================
open(F,">./smooth.c");
print F "//==================================================================================================\n";
$GlobalU = 16;
$DRAM_SCOUT_THREADS     = 0; # MUST BE A MULTIPLE OF 4 !!!  (n.b. doesn't help in the 4x16 case)
#if($DRAM_SCOUT_THREADS % 2){
#  print "ERROR, DRAM_SCOUT_THREADS($DRAM_SCOUT_THREADS) must be a multiple of 2\n";
#  exit(0);
#}
&smooth_gsrb_wavefront($GlobalU,0);
&smooth_gsrb_wavefront($GlobalU,1);
print F "//==================================================================================================\n";
print F "void smooth(domain_type * domain, int level, int phi_id, int rhs_id, double a, double b, double hLevel, int sweep){\n";
print F "  int CollaborativeThreadingBoxSize = 100000; // i.e. never\n";
print F "  #ifdef __COLLABORATIVE_THREADING\n";
print F "    #warning using Collaborative Threading for large boxes...\n";
print F "    CollaborativeThreadingBoxSize = 1 << __COLLABORATIVE_THREADING;\n";
print F "  #endif\n";
print F "  int box;\n";
print F "  if(domain->subdomains[0].levels[level].dim.i >= CollaborativeThreadingBoxSize){\n";
print F "    for(box=0;box<domain->numsubdomains;box++){__box_smooth_GSRB_multiple_threaded(&domain->subdomains[box].levels[level],phi_id,rhs_id,a,b,hLevel,sweep);}\n";
print F "  }else{\n";
print F "    #pragma omp parallel for private(box)\n";
print F "    for(box=0;box<domain->numsubdomains;box++){__box_smooth_GSRB_multiple(&domain->subdomains[box].levels[level],phi_id,rhs_id,a,b,hLevel,sweep);}\n";
print F "  }\n";
print F "}\n";
print F "//==================================================================================================\n";
close(F);
 

#==========================================================================================================================================
sub smooth_gsrb_wavefront{
  local($U,$THREADED)=@_;
  $Alignment = 4; # i.e. align to multiples of four...
  $AlignmentMinus1 = $Alignment-1;
  if($THREADED){$FunctionName = "__box_smooth_GSRB_multiple_threaded";}
           else{$FunctionName = "__box_smooth_GSRB_multiple";}
  print F "void $FunctionName(box_type *box, int phi_id, int rhs_id, double a, double b, double h, int sweep){\n";



 
  if($THREADED){
  print F "  #pragma omp parallel\n";
  print F "  {\n";
  print F "    int pencil = box->pencil;\n";
  print F "    int plane = box->plane;\n";
  print F "    int plane8   = plane<<3;\n";
  print F "    int pencil8  = pencil<<3;\n";
  print F "    int ghosts = box->ghosts;\n";
  print F "    int DimI = box->dim.i;\n";
  print F "    int DimJ = box->dim.j;\n";
  print F "    int DimK = box->dim.k;\n";
  print F "    double h2inv = 1.0/(h*h);\n";
  print F "    double  * __restrict__ phi          = box->grids[  phi_id] + ghosts*plane;\n";
  print F "    double  * __restrict__ rhs          = box->grids[  rhs_id] + ghosts*plane;\n";
  print F "    double  * __restrict__ alpha        = box->grids[__alpha ] + ghosts*plane;\n";
  print F "    double  * __restrict__ beta_i       = box->grids[__beta_i] + ghosts*plane;\n";
  print F "    double  * __restrict__ beta_j       = box->grids[__beta_j] + ghosts*plane;\n";
  print F "    double  * __restrict__ beta_k       = box->grids[__beta_k] + ghosts*plane;\n";
  print F "    double  * __restrict__ lambda       = box->grids[__lambda] + ghosts*plane;\n";
  print F "    uint64_t* __restrict__ RedBlackMask = box->RedBlackMask;\n";
  print F "    const __m256d       a_splat4 =               _mm256_broadcast_sd(&a);\n";
  print F "    const __m256d b_h2inv_splat4 = _mm256_mul_pd(_mm256_broadcast_sd(&b),_mm256_broadcast_sd(&h2inv));\n";
  print F "    int id      = omp_get_thread_num();\n";
  print F "    int threads = omp_get_num_threads();\n";
  print F "    int DRAM_scout_threads = $DRAM_SCOUT_THREADS;\n";
  print F "    int global_ij_start[8];\n";
  print F "    int global_ij_end[8];\n";
  print F "    int ij_start[8];\n";
  print F "    int ij_end[8];\n";
  print F "    int planeInWavefront;for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){\n";
  # ERROR, this will cause a data hazard...
  #print F "      global_ij_start[planeInWavefront] = (                   (1+planeInWavefront)*pencil)&~$AlignmentMinus1;\n";
  #print F "      global_ij_end[planeInWavefront]   = ((ghosts+DimJ+ghosts-1-planeInWavefront)*pencil);\n";
  print F "      global_ij_start[planeInWavefront] = (                   (1)*pencil)&~$AlignmentMinus1;\n";
  print F "      global_ij_end[planeInWavefront]   = ((ghosts+DimJ+ghosts-1)*pencil);\n";
  # FIX, trim if first or last team...
  print F "      int TotalUnrollings = ((global_ij_end[planeInWavefront]-global_ij_start[planeInWavefront]+$U-1)/$U);\n";
  print F "      ij_start[planeInWavefront] = global_ij_start[planeInWavefront] + $U*( (id          )*(TotalUnrollings)/(threads-DRAM_scout_threads));\n";
  print F "      ij_end[planeInWavefront]   = global_ij_start[planeInWavefront] + $U*( (id+1        )*(TotalUnrollings)/(threads-DRAM_scout_threads));\n";
  if($DRAM_SCOUT_THREADS){
  print F "      if(id>=(threads-DRAM_scout_threads)){\n";
  print F "        ij_start[planeInWavefront] = -32+global_ij_start[planeInWavefront] + pencil + (plane-pencil)*(id  -(threads-DRAM_scout_threads))/DRAM_scout_threads;\n";
  print F "        ij_end[planeInWavefront]   = -32+global_ij_start[planeInWavefront] + pencil + (plane-pencil)*(id+1-(threads-DRAM_scout_threads))/DRAM_scout_threads;\n";
  print F "      }\n";
  }
  #print F "      if(id>=(threads-1-DRAM_scout_threads))ij_end[planeInWavefront] = global_ij_end[planeInWavefront];\n";
  print F "      if(ij_end[planeInWavefront]>global_ij_end[planeInWavefront])ij_end[planeInWavefront]=global_ij_end[planeInWavefront];\n"; 
  print F "    }\n";





 
  }else{ # sequential version...
 #print F "  {\n";
  print F "    int pencil = box->pencil;\n";
  print F "    int plane = box->plane;\n";
  print F "    int plane8   = plane<<3;\n";
  print F "    int pencil8  = pencil<<3;\n";
  print F "    int ghosts = box->ghosts;\n";
  print F "    int DimI = box->dim.i;\n";
  print F "    int DimJ = box->dim.j;\n";
  print F "    int DimK = box->dim.k;\n";
  print F "    double h2inv = 1.0/(h*h);\n";
  print F "    double  * __restrict__ phi          = box->grids[  phi_id] + ghosts*plane;\n";
  print F "    double  * __restrict__ rhs          = box->grids[  rhs_id] + ghosts*plane;\n";
  print F "    double  * __restrict__ alpha        = box->grids[__alpha ] + ghosts*plane;\n";
  print F "    double  * __restrict__ beta_i       = box->grids[__beta_i] + ghosts*plane;\n";
  print F "    double  * __restrict__ beta_j       = box->grids[__beta_j] + ghosts*plane;\n";
  print F "    double  * __restrict__ beta_k       = box->grids[__beta_k] + ghosts*plane;\n";
  print F "    double  * __restrict__ lambda       = box->grids[__lambda] + ghosts*plane;\n";
  print F "    uint64_t* __restrict__ RedBlackMask = box->RedBlackMask;\n";
  print F "    const __m256d       a_splat4 =               _mm256_broadcast_sd(&a);\n";
  print F "    const __m256d b_h2inv_splat4 = _mm256_mul_pd(_mm256_broadcast_sd(&b),_mm256_broadcast_sd(&h2inv));\n";
  print F "    int global_ij_start[8];\n";
  print F "    int global_ij_end[8];\n";
  print F "    int ij_start[8];\n";
  print F "    int ij_end[8];\n";
  print F "    int planeInWavefront;for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){\n";
  print F "      global_ij_start[planeInWavefront] = (                   (1+planeInWavefront)*pencil)&~$AlignmentMinus1;\n";
  print F "      global_ij_end[planeInWavefront]   = ((ghosts+DimJ+ghosts-1-planeInWavefront)*pencil);\n";
 #print F "      global_ij_end[planeInWavefront]    = global_ij_start[planeInWavefront]+$U*((((ghosts+DimJ+ghosts-1)*pencil-1-planeInWavefront)-global_ij_start[planeInWavefront]+$U-1)/$U);\n";
  print F "      ij_start[planeInWavefront] = global_ij_start[planeInWavefront];\n";
  print F "      ij_end[planeInWavefront]   = global_ij_end[planeInWavefront];\n";
  print F "    }\n";
  }





  #if($THREADED && $DRAM_SCOUT_THREADS){
  #print F "      double * __restrict__ Prefetch_Pointers[20];\n";
  #print F "                        int Prefetch_ij_start[20];\n";
  #print F "                        int Prefetch_ij_end[  20];\n";
  #$PF_Streams=0;
  #print F "    if(id>=(threads-DRAM_scout_threads)){\n";
  #print F "      Prefetch_Pointers[$PF_Streams] =    phi-plane;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane-pencil;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] =    phi      ;Prefetch_ij_start[$PF_Streams]=     0;Prefetch_ij_end[$PF_Streams]=plane       ;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] =    phi+plane;Prefetch_ij_start[$PF_Streams]=     0;Prefetch_ij_end[$PF_Streams]=plane       ;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] = beta_k+plane;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane-pencil;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] = beta_k      ;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane-pencil;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] = beta_j      ;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane       ;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] = beta_i      ;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane       ;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] =  alpha      ;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane-pencil;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] =    rhs      ;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane-pencil;\n";$PF_Streams++;
  #print F "      Prefetch_Pointers[$PF_Streams] = lambda      ;Prefetch_ij_start[$PF_Streams]=pencil;Prefetch_ij_end[$PF_Streams]=plane-pencil;\n";$PF_Streams++;
  #print F "    }\n";
  #}

  print F "   #if defined(__PREFETCH_NEXT_PLANE_FROM_DRAM)\n";
  $PF_Streams=0;
  print F "   double * __restrict__ Prefetch_Pointers[20];\n";
  print F "   Prefetch_Pointers[$PF_Streams] =    phi+plane-pencil;\n";$PF_Streams++;
  print F "   Prefetch_Pointers[$PF_Streams] = beta_k+plane       ;\n";$PF_Streams++;
  print F "   Prefetch_Pointers[$PF_Streams] = beta_j             ;\n";$PF_Streams++;
  print F "   Prefetch_Pointers[$PF_Streams] = beta_i             ;\n";$PF_Streams++;
  print F "   Prefetch_Pointers[$PF_Streams] =  alpha             ;\n";$PF_Streams++;
  print F "   Prefetch_Pointers[$PF_Streams] =    rhs             ;\n";$PF_Streams++;
  print F "   Prefetch_Pointers[$PF_Streams] = lambda             ;\n";$PF_Streams++;
  print F "   #endif\n";

  print F "    int leadingK;\n";
  print F "    int kLow  =     -(ghosts-1);\n";
  print F "    int kHigh = DimK+(ghosts-1);\n";

  print F "    for(leadingK=kLow-1;leadingK<kHigh;leadingK++){\n";

  if($THREADED){
  print F "      if(ghosts>1){\n";
  #print F "        printf(\"%03d: %04d..%04d\\n\",id,ij_start[0],ij_end[0]);\n";
  print F "        #pragma omp barrier\n";
  #print F "        exit(0);\n";
  print F "      }\n";
  }

  #if($THREADED && $DRAM_SCOUT_THREADS){
  #print F "      if(id>=(threads-DRAM_scout_threads)){\n";
  #print F "        #warning using scout cores to prefetch the next plane from DRAM\n";
  #print F "        int ij,prefetch_stream;\n";
  #print F "        int prefetch_stream_high = $PF_Streams;\n";
  #print F "        for(prefetch_stream=id-(threads-DRAM_scout_threads);prefetch_stream<prefetch_stream_high;prefetch_stream+=DRAM_scout_threads){\n";
  #print F "          double * _base = Prefetch_Pointers[prefetch_stream] + (leadingK+1)*plane;\n";
  #print F "          for(ij=Prefetch_ij_start[prefetch_stream];ij<Prefetch_ij_end[prefetch_stream];ij+=32){ // prefetch...\n";
  #print F "            _mm_prefetch((const char*)(_base+ij+ 0),_MM_HINT_T2);\n";
  #print F "            _mm_prefetch((const char*)(_base+ij+ 8),_MM_HINT_T2);\n";
  #print F "            _mm_prefetch((const char*)(_base+ij+16),_MM_HINT_T2);\n";
  #print F "            _mm_prefetch((const char*)(_base+ij+24),_MM_HINT_T2);\n";
  #print F "      }}}else\n";
  #}
  if($THREADED && $DRAM_SCOUT_THREADS){
  print F "      if(id>=(threads-DRAM_scout_threads)){\n";
  print F "        #warning using scout cores to prefetch the next plane from DRAM\n";
  print F "        int k=(leadingK+1);\n";
  print F "        int ij        = ij_start[0]        ;\n";
  print F "        int ijk_start = ij_start[0]+k*plane;\n";
  print F "        int ijk       = ijk_start;\n";
  print F "        int _ij_end   = ij_end[0];\n";
  print F "        while(ij<_ij_end){ // prefetch a vector...\n";
  print F "            _mm_prefetch((const char*)(   phi+ijk+ plane-pencil),_MM_HINT_T2);\n";
  print F "            _mm_prefetch((const char*)(beta_k+ijk+ plane       ),_MM_HINT_T2);\n";
  print F "            _mm_prefetch((const char*)(beta_j+ijk              ),_MM_HINT_T2);\n";
  print F "            _mm_prefetch((const char*)(beta_i+ijk              ),_MM_HINT_T2);\n";
  print F "            _mm_prefetch((const char*)( alpha+ijk              ),_MM_HINT_T2);\n";
  print F "            _mm_prefetch((const char*)(   rhs+ijk              ),_MM_HINT_T2);\n";
  print F "            _mm_prefetch((const char*)(lambda+ijk              ),_MM_HINT_T2);\n";
  print F "            ij +=8;\n";
  print F "            ijk+=8;\n";
  print F "      }}else\n";
  }

  print F "      {\n";
  print F "        #if defined(__PREFETCH_NEXT_PLANE_FROM_DRAM)\n";
  print F "        int prefetch_stream=0;\n";
  print F "        int prefetch_ijk_start = ij_start[0] + (leadingK+1)*plane;\n";
  print F "        int prefetch_ijk_end   = ij_end[0]   + (leadingK+1)*plane;\n";
  print F "        int prefetch_ijk       = prefetch_ijk_start;\n";
  print F "        #endif\n";
  print F "        int j,k;\n";
  print F "        for(planeInWavefront=0;planeInWavefront<ghosts;planeInWavefront++){\n";
  print F "          k=(leadingK-planeInWavefront);if((k>=kLow)&&(k<kHigh)){\n";
  $GlobalS=$GlobalU;
                     &smooth_gsrb_VL_avx($GlobalU,$GlobalS);
  print F "        }}\n";
  print F "      } // if stencil\n";

  print F "    } // leadingK\n";
  if($THREADED){
  print F "  } // omp parallel region\n";
  }
  print F "}\n\n\n";
}


#==========================================================================================================================================
sub smooth_gsrb_VL_avx{
  local($U,$S)=@_;

                        print  F "        uint64_t invertMask = 0-((k^planeInWavefront^sweep^1)&0x1);\n";
                        print  F "        const __m256d    invertMask4 =               _mm256_broadcast_sd((double*)&invertMask);\n";
                        #print  F "        int ij           = ij_start[planeInWavefront];\n";
                        #print  F "        int ijk_start    = ij_start[planeInWavefront]+k*plane;\n";
                        #print  F "        int ijk          = ijk_start;\n";
                        print  F "        int ij,kplane=k*plane;\n";
                        print  F "        int _ij_start    = ij_start[planeInWavefront];\n";
                        print  F "        int _ij_end      = ij_end[planeInWavefront];\n";

                        #print  F "        while(ij<_ij_end){ // smooth an interleaved vector...\n";
                        #print  F "        for(ij=ij_start[planeInWavefront];ij<ij_end[planeInWavefront];ij+=$S){ // smooth a vector...\n";
                        print  F "        for(ij=_ij_start;ij<_ij_end;ij+=$S){ // smooth a vector...\n";
                        print  F "          int ijk=ij+kplane;\n";

                        $PF_number    = int(($PF_Streams*$U+31)/32); #   7 ($PF_Streams) planes prefetched for every 4 planes computed
                        $PF_increment = int(($PF_Streams*$U   )/4);  # exact increment... e.g. increment by 14 == redundant prefetches
                       #$PF_increment = 8*$PF_number;                # increment matches number of prefetches == too many prefetches
                        print  F "          #if defined(__PREFETCH_NEXT_PLANE_FROM_DRAM)\n";
                        print  F "          #warning will attempt to prefetch the next plane from DRAM one component at a time\n";
                        printf(F "            double * _base = Prefetch_Pointers[prefetch_stream] + prefetch_ijk;\n");
for($x=0;$x<8*$PF_number;$x+=8){
                        printf(F "            _mm_prefetch((const char*)(_base+%2d),_MM_HINT_T1);\n",$x);}
                        printf(F "            prefetch_ijk+=$PF_increment;if(prefetch_ijk>prefetch_ijk_end){prefetch_stream++;prefetch_ijk=prefetch_ijk_start;}\n");
                        print  F "          #endif\n";



  for($x=0;$x<$U;$x+=4){printf(F "                __m256d helmholtz_%02d;\n",$x);}


                              printf(F "          #if 1 // this version performs alligned accesses for phi+/-1, but not betai+1 or phi+/-pencil\n",$x);
  for($x=0;$x<$U+8;$x+=4){ # pipelined loads/shuffles of phi
    if(($x>=   0)&&($x<$U+2)){printf(F "          const __m128d      temp_%02d = _mm_load_pd(phi+ijk+%3d);\n",$x,$x-2);}
    if(($x>=   0)&&($x<$U+2)){printf(F "          const __m128d      temp_%02d = _mm_load_pd(phi+ijk+%3d);\n",$x+2,$x);}
    if(($x>=   4)&&($x<$U+2)){printf(F "          const __m128d      temp_%02d = _mm_shuffle_pd(temp_%02d,temp_%02d,1);\n",$x-1,$x-2,$x+0);}
    if(($x>=   0)&&($x<$U+2)){printf(F "          const __m128d      temp_%02d = _mm_shuffle_pd(temp_%02d,temp_%02d,1);\n",$x+1,$x+0,$x+2);}
    if(($x==   4)           ){
                        printf(F "          const __m256d       phi_%02d = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_%02d),temp_%02d,1);\n",$x-4,$x-2,$x+0);
                        printf(F "          const __m256d       phi_%02s = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_%02d),temp_%02d,1);\n","m1",$x-3,$x-1);
                        printf(F "          const __m256d       phi_%02d = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_%02d),temp_%02d,1);\n",$x-3,$x-1,$x+1);}
    if(($x>=   8)&&($x<$U+4)){
                        printf(F "          const __m256d       phi_%02d = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_%02d),temp_%02d,1);\n",$x-4,$x-2,$x+0);
                        printf(F "          const __m256d       phi_%02d = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_%02d),temp_%02d,1);\n",$x-5,$x-3,$x-1);
                        printf(F "          const __m256d       phi_%02d = _mm256_insertf128_pd(_mm256_castpd128_pd256(temp_%02d),temp_%02d,1);\n",$x-3,$x-1,$x+1);}
  }
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d =                              _mm256_mul_pd(_mm256_sub_pd(phi_%02d,phi_%02d),_mm256_loadu_pd(beta_i+ijk+       %2d)); \n",$x,$x+1,$x,$x+1);}
                   $x=0;printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(phi_%02d,phi_%02s),_mm256_load_pd( beta_i+ijk+       %2d)));\n",$x,$x,$x,"m1",$x);
  for($x=4;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(phi_%02d,phi_%02d),_mm256_load_pd( beta_i+ijk+       %2d)));\n",$x,$x,$x,$x-1,$x);}
                        printf(F "                        //careful... assumes the compiler maps _mm256_load_pd to unaligned vmovupd and not the aligned version (should be faster when pencil is a multiple of 4 doubles (32 bytes)\n");
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_add_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+pencil+%2d),                phi_%02d           ),_mm256_load_pd( beta_j+ijk+pencil+%2d)));\n",$x,$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(                phi_%02d           ,_mm256_load_pd( phi+ijk-pencil+%2d)),_mm256_load_pd( beta_j+ijk+       %2d)));\n",$x,$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_add_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+%2d),                phi_%02d           ),_mm256_load_pd( beta_k+ijk+ plane+%2d)));\n",$x,$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(                phi_%02d           ,_mm256_load_pd( phi+ijk- plane+%2d)),_mm256_load_pd( beta_k+ijk       +%2d)));\n",$x,$x,$x,$x,$x);}

                        printf(F "          #else // this version performs unalligned accesses for phi+/-1, betai+1 and phi+/-pencil\n",$x);
  for($x=0;$x<$U;$x+=4){printf(F "          const __m256d       phi_%02d = _mm256_load_pd(phi+ijk+%3d);\n",$x,$x);}
                        printf(F "                        //careful... assumes the compiler maps _mm256_load_pd to unaligned vmovupd and not the aligned version (should be faster when pencil is a multiple of 4 doubles (32 bytes)\n");
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d =                              _mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd(phi+ijk+       %2d),                phi_%02d           ),_mm256_load_pd(beta_i+ijk+       %2d)); \n",$x,$x+1,$x,$x+1);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(                phi_%02d           ,_mm256_load_pd(phi+ijk+       %2d)),_mm256_load_pd( beta_i+ijk+       %2d)));\n",$x,$x,$x,$x-1,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_add_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(_mm256_loadu_pd( phi+ijk+pencil+%2d),                phi_%02d           ),_mm256_loadu_pd( beta_j+ijk+pencil+%2d)));\n",$x,$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(                phi_%02d           ,_mm256_loadu_pd( phi+ijk-pencil+%2d)),_mm256_load_pd( beta_j+ijk+       %2d)));\n",$x,$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_add_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(_mm256_load_pd( phi+ijk+ plane+%2d),                phi_%02d           ),_mm256_load_pd( beta_k+ijk+ plane+%2d)));\n",$x,$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(helmholtz_%02d,_mm256_mul_pd(_mm256_sub_pd(                phi_%02d           ,_mm256_load_pd( phi+ijk- plane+%2d)),_mm256_load_pd( beta_k+ijk       +%2d)));\n",$x,$x,$x,$x,$x);}

                        printf(F "          #endif\n",$x);
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_mul_pd(helmholtz_%02d,b_h2inv_splat4);\n",$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                        helmholtz_%02d = _mm256_sub_pd(_mm256_mul_pd(_mm256_mul_pd(a_splat4,_mm256_load_pd(alpha+ijk+%2d)),phi_%02d),helmholtz_%02d);\n",$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                __m256d       new_%02d = _mm256_mul_pd(_mm256_load_pd(lambda+ijk+%2d),_mm256_sub_pd(helmholtz_%02d,_mm256_load_pd(rhs+ijk+%2d)));\n",$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                              new_%02d = _mm256_sub_pd(phi_%02d,new_%02d);\n",$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "           const __m256d RedBlack_%02d = _mm256_xor_pd(invertMask4,_mm256_castsi256_pd(_mm256_load_si256( (__m256i*)(RedBlackMask+ij+%2d) )));\n",$x,$x);}
 #for($x=0;$x<$U;$x+=4){printf(F "           const __m256d RedBlack_%02d = _mm256_xor_pd(invertMask4,_mm256_load_pd( (double*)(RedBlackMask+ij+%2d) ));\n",$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                             new_%02d  = _mm256_blendv_pd(phi_%02d,new_%02d,RedBlack_%02d);\n",$x,$x,$x,$x);}
  for($x=0;$x<$U;$x+=4){printf(F "                                         _mm256_store_pd(phi+ijk+%2d,new_%02d);\n",$x,$x);}

                         #print  F "          ij+=$S;ijk+=$S;\n";
                         print F "        }\n";

return;
}
#==========================================================================================================================================
